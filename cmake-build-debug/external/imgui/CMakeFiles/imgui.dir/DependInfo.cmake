# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/dmitrybelyaev/opengl_tasks2019/external/imgui/imgui.cpp" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/imgui/CMakeFiles/imgui.dir/imgui.cpp.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/imgui/imgui_demo.cpp" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/imgui/CMakeFiles/imgui.dir/imgui_demo.cpp.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/imgui/imgui_draw.cpp" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/imgui/CMakeFiles/imgui.dir/imgui_draw.cpp.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/imgui/imgui_impl_glfw_gl3.cpp" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/imgui/CMakeFiles/imgui.dir/imgui_impl_glfw_gl3.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLFW_DLL"
  "GLM_FORCE_DEPTH_ZERO_TO_ONE"
  "GLM_FORCE_PURE"
  "GLM_FORCE_RADIANS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../external/imgui"
  "../external/glew-1.13.0/include"
  "../external/GLFW/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/glew-1.13.0/build/cmake/CMakeFiles/glew_s.dir/DependInfo.cmake"
  "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
