# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/cocoa_init.m" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/cocoa_init.m.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/cocoa_joystick.m" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/cocoa_joystick.m.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/cocoa_monitor.m" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/cocoa_monitor.m.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/cocoa_time.c" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/cocoa_time.c.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/cocoa_window.m" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/cocoa_window.m.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/context.c" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/context.c.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/init.c" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/init.c.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/input.c" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/input.c.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/monitor.c" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/monitor.c.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/nsgl_context.m" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/nsgl_context.m.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/posix_tls.c" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/posix_tls.c.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/vulkan.c" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/vulkan.c.o"
  "/Users/dmitrybelyaev/opengl_tasks2019/external/GLFW/src/window.c" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/external/GLFW/src/CMakeFiles/glfw.dir/window.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLM_FORCE_DEPTH_ZERO_TO_ONE"
  "GLM_FORCE_PURE"
  "GLM_FORCE_RADIANS"
  "_GLFW_USE_CONFIG_H"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../external/GLFW/include"
  "../external/GLFW/src"
  "external/GLFW/src"
  )

# Pairs of files generated by the same build rule.
set(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/libglfwd.3.dylib" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/libglfwd.3.2.dylib"
  "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/libglfwd.dylib" "/Users/dmitrybelyaev/opengl_tasks2019/cmake-build-debug/libglfwd.3.2.dylib"
  )


# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
