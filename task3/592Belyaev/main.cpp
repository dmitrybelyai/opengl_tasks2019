#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <vector>

#include <glm/gtc/constants.hpp>

MeshPtr createMesh(std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals,
                   std::vector<glm::vec2> &texture_coords) {
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texture_coords.size() * sizeof(float) * 2, texture_coords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    return mesh;
}


// Функция создания веток для дерева

void addCone(float height, float radius, int polygonesCount, glm::mat4 modelMatrix,
             std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals, std::vector<glm::vec2> &texture_coords) {
    glm::vec3 coneTop = glm::vec3(0.0f, 0.0f, height);

    for (int i = 0; i < polygonesCount; ++i) {
        float angle1 = glm::two_pi<float>() * i / polygonesCount;
        glm::vec3 v1 = radius * glm::vec3(glm::cos(angle1), glm::sin(angle1), 0.0f);

        float angle2 = glm::two_pi<float>() * (i + 1) / polygonesCount;
        glm::vec3 v2 = radius * glm::vec3(glm::cos(angle2), glm::sin(angle2), 0.0f);

        // боковая поверхность
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.push_back(coneTop);

        normals.push_back(glm::normalize(v1));
        normals.push_back(glm::normalize(v2));
        normals.push_back(glm::normalize(coneTop));

        texture_coords.emplace_back(glm::vec2((0.0 + i) / polygonesCount, 0));
        texture_coords.emplace_back(glm::vec2((1.0 + i) / polygonesCount, 0));
        texture_coords.emplace_back(glm::vec2((0.5 + i) / polygonesCount, 10));

        // основание
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.emplace_back(glm::vec3(0.0f, 0.0f, 0.0f));

        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
        normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));

        texture_coords.emplace_back(glm::vec2(0, 0));
        texture_coords.emplace_back(glm::vec2(0, 0));
        texture_coords.emplace_back(glm::vec2(0, 0));
    }

    for (int i = (int)vertices.size() - polygonesCount * 6; i < vertices.size(); ++i) {
        vertices[i] = modelMatrix * glm::vec4(vertices[i], 1.0);
        normals[i] = modelMatrix * glm::vec4(normals[i], 1.0);
    }
}

// Функция для формирования векторов вершин, векторов нормалей и координат текстур для полигонов

void addRect(float width, float height, float textureScale, glm::mat4 modelMatrix,
             std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals, std::vector<glm::vec2> &texture_coords) {
    //front 1
    vertices.emplace_back(glm::vec3(-width / 2, height / 2, 0.0));
    vertices.emplace_back(glm::vec3(width / 2, -height / 2, 0.0));
    vertices.emplace_back(glm::vec3(width / 2, height / 2, 0.0));

    normals.emplace_back(glm::vec3(0.0, 0.0, 1.0));
    normals.emplace_back(glm::vec3(0.0, 0.0, 1.0));
    normals.emplace_back(glm::vec3(0.0, 0.0, 1.0));

    texture_coords.emplace_back(glm::vec2(0, textureScale));
    texture_coords.emplace_back(glm::vec2(textureScale, 0));
    texture_coords.emplace_back(glm::vec2(textureScale, textureScale));

    //front 2
    vertices.emplace_back(glm::vec3(-width / 2, height / 2, 0.0));
    vertices.emplace_back(glm::vec3(-width / 2, -height / 2, 0.0));
    vertices.emplace_back(glm::vec3(width / 2, -height / 2, 0.0));

    normals.emplace_back(glm::vec3(0.0, 0.0, 1.0));
    normals.emplace_back(glm::vec3(0.0, 0.0, 1.0));
    normals.emplace_back(glm::vec3(0.0, 0.0, 1.0));

    texture_coords.emplace_back(glm::vec2(0, textureScale));
    texture_coords.emplace_back(glm::vec2(0, 0));
    texture_coords.emplace_back(glm::vec2(textureScale, 0));

    for (int i = (int)vertices.size() - 6; i < vertices.size(); ++i) {
        vertices[i] = modelMatrix * glm::vec4(vertices[i], 1.0);
        normals[i] = modelMatrix * glm::vec4(normals[i], 1.0);
    }
}

// Используется, чтобы добавить плоскость(землю)
MeshPtr createRect(float width, float height, float textureScale = 1.0) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texture_coords;

    addRect(width, height, textureScale, glm::mat4(1.0), vertices, normals, texture_coords);

    return createMesh(vertices, normals, texture_coords);
}


class Tree {
public:

    void Build(MeshPtr &trunk, MeshPtr &leafs, float &height, glm::mat4 modelMatrix = glm::mat4(1.0f)) {
        trunkVertices.clear();
        trunkNormals.clear();
        trunkTextureCoords.clear();

        leafsVertices.clear();
        leafsNormals.clear();
        leafsTextureCoords.clear();

        build(1, 0.1, 2, modelMatrix);

        height = 0;
        for (glm::vec3 v : leafsVertices) {
            if (height < v[2]) {
                height = v[2];
            }
        }

        trunk = createMesh(trunkVertices, trunkNormals, trunkTextureCoords);
        leafs = createMesh(leafsVertices, leafsNormals, leafsTextureCoords);

        trunk->setModelMatrix(glm::mat4(1.0));
        leafs->setModelMatrix(glm::mat4(1.0));
    }

private:
    std::vector<glm::vec3> trunkVertices;
    std::vector<glm::vec3> trunkNormals;
    std::vector<glm::vec2> trunkTextureCoords;

    std::vector<glm::vec3> leafsVertices;
    std::vector<glm::vec3> leafsNormals;
    std::vector<glm::vec2> leafsTextureCoords;

    void createLeafes(float radius, float length, glm::mat4 mat) {
        int n_leafs = 10 + rand() % 10;
        for (int i = 0; i < n_leafs; ++i) {
            float width = 0.03;
            float height = 0.05;
            float displacement = length * (0.3f + 1.0f * (rand() % 70) / 100);
            float distance = radius * (1.0f + (rand() % 200) / 100);
            float alpha = glm::radians(1.0f * (rand() % 360));
            float beta = glm::radians(-30.0f + rand() % 100);

            glm::mat4 leafMat = glm::rotate(glm::mat4(1), alpha, glm::vec3(0.0f, 0.0f, 1.0f));
            leafMat = glm::translate(leafMat, glm::vec3(distance, 0, displacement));
            leafMat = glm::rotate(leafMat, beta, glm::vec3(1.0f, 0.0f, 0.0f));
            leafMat = glm::translate(leafMat, glm::vec3(0, height / 2, 0));

            addRect(width, height, 1.0, mat * leafMat, leafsVertices, leafsNormals, leafsTextureCoords);
        }
    }

    void build(int depth, float radius, float length, glm::mat4 mat) {
        if (depth > 5) {
            return;
        }
        // Добавляем листья в дереве только на этом уровне
        if (depth > 3 && depth <= 6) {
            createLeafes(radius, length, mat);
        }

        // Количество полигонов в модели ветки дерева
        int n = 200 / (depth * glm::log(1 + depth));

        addCone(length, radius, n, mat, trunkVertices, trunkNormals, trunkTextureCoords);

        for (int level = 1; level <= 3; ++level) {
            // n_children - ветвистость дерева
            int n_children = 5;
            for (int i = 0; i < n_children; ++i) {
                float h = length * (0.15f + 0.2f * level + 1.0f * (rand() % 20) / 100);
                float r = radius * (1.0f - h / length) * (0.9f - 1.0f * (rand() % 20) / 100);
                float l = length * (0.7f - 1.0f * (rand() % 20) / 100) / level;
                float alpha = glm::radians(360.0f * i / n_children + rand() % 10);
                float beta = glm::radians(30.0f + rand() % 10);

                glm::mat4 childMat = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, h));
                childMat = glm::rotate(childMat, alpha, glm::vec3(0.0f, 0.0f, 1.0f));
                childMat = glm::rotate(childMat, beta, glm::vec3(1.0f, 0.0f, 0.0f));

                build(depth + level, r, l, mat * childMat);
            }
        }
    }
};


class TreeApp : public Application {
public:
    MeshPtr trunk;
    MeshPtr leafs;
    MeshPtr grass;

    std::vector<glm::vec3> treeModelPos;
    DataBufferPtr treeModelPosBuf;
    float groundSizeA = 10;
    float groundSizeB = 20;
    int nTrees = 1;
    float prob = 0.2;
    float treeRadius = 1.0;
    float treeHeight;

    ShaderProgramPtr shader;
    ShaderProgramPtr shaderInstanced;
    ShaderProgramPtr cullShader;

    TexturePtr barkTexture;
    TexturePtr leafTexture;
    TexturePtr grassTexture;

    TexturePtr bufferTexAll;
    TexturePtr bufferTexCulled;

    GLuint sampler;

    //Переменные для источника света
    float lr = 10.0;
    float phi = glm::pi<float>() * 0.5f;
    float theta = glm::pi<float>() * 0.25f;

    LightInfo light;

    GLuint TF; //Объект для хранения настроект Transform Feedback
    GLuint cullVao; //VAO, который хранит настройки буфера для чтения данных во время Transform Feedback
    GLuint tfOutputVbo; //VBO, в который будут записываться смещения моделей после отсечения

    GLuint query; //Переменная-счетчик, куда будет записываться количество пройденных отбор моделей

    void makeScene() override {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        Tree().Build(trunk, leafs, treeHeight);

        nTrees = 1;
        int A = groundSizeA / (treeRadius * 2);
        int B = groundSizeB / (treeRadius * 2);
        for (int i = 0; i < A; ++i) {
            for (int j = 0; j < B; ++j) {
                if (1.0f * (rand() % 100) / 100 < prob) {
                    float a = treeRadius * 2 * i + treeRadius - groundSizeA / 2 +
                              0.5f * treeRadius * (rand() % 100) / 100;
                    float b = treeRadius * 2 * j + treeRadius - groundSizeB / 2 +
                              0.5f * treeRadius * (rand() % 100) / 100;

                    treeModelPos.emplace_back(glm::vec3(a, b, 0.0));
                    nTrees += 1;
                }
            }
        }

        treeModelPosBuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        treeModelPosBuf->setData(nTrees * sizeof(float) * 3, treeModelPos.data());

        glGenBuffers(1, &tfOutputVbo);
        glBindBuffer(GL_ARRAY_BUFFER, tfOutputVbo);
        glBufferData(GL_ARRAY_BUFFER, nTrees * sizeof(float) * 3, 0, GL_STREAM_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        //Создаем текстурный буфер и привязываем к нему буфер без выравнивания
        bufferTexAll = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        bufferTexAll->bind();
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, treeModelPosBuf->id());
        bufferTexAll->unbind();

        //Создаем текстурный буфер и привязываем к нему буфер без выравнивания
        bufferTexCulled = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
        bufferTexCulled->bind();
        glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, tfOutputVbo);
        bufferTexCulled->unbind();

        grass = createRect(groundSizeA, groundSizeB, 100);
        grass->setModelMatrix(glm::mat4(1));

        shader = std::make_shared<ShaderProgram>("592BelyaevData3/shader.vert", "592BelyaevData3/shader.frag");
        shaderInstanced = std::make_shared<ShaderProgram>("592BelyaevData3/instancingTexture.vert",
                                                           "592BelyaevData3/shader.frag");

        cullShader = std::make_shared<ShaderProgram>();

        ShaderPtr vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
        vs->createFromFile("592BelyaevData3/cull.vert");
        cullShader->attachShader(vs);

        ShaderPtr gs = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
        gs->createFromFile("592BelyaevData3/cull.geom");
        cullShader->attachShader(gs);

        const char *attribs[] = {"position"};
        glTransformFeedbackVaryings(cullShader->id(), 1, attribs, GL_SEPARATE_ATTRIBS);

        cullShader->linkProgram();

        glGenVertexArrays(1, &cullVao);
        glBindVertexArray(cullVao);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, treeModelPosBuf->id());
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindVertexArray(0);

        glGenTransformFeedbacks(1, &TF);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, TF);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, tfOutputVbo);
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

        glGenQueries(1, &query);

        leafTexture = loadTexture("592BelyaevData3/leaf.png");
        barkTexture = loadTexture("592BelyaevData3/bark.jpg");
        grassTexture = loadTexture("592BelyaevData3/grass.jpg");

        light.position =
                glm::vec3(glm::cos(phi) * glm::cos(theta), glm::sin(phi) * glm::cos(theta), glm::sin(theta)) * lr;
        light.ambient = glm::vec3(0.2, 0.2, 0.2);
        light.diffuse = glm::vec3(0.9, 0.9, 0.9);
        light.specular = glm::vec3(0.05, 0.05, 0.05);

        glGenSamplers(1, &sampler);
        glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light")) {
                ImGui::ColorEdit3("ambient", glm::value_ptr(light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(light.specular));

                ImGui::SliderFloat("radius", &lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        drawGrass();

        cull(cullShader);
        drawTrees();
    }

    void drawGrass() {
        shader->use();

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        light.position = glm::vec3(glm::cos(phi) * glm::cos(theta),
                                    glm::sin(phi) * glm::cos(theta), glm::sin(theta)) * lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(light.position, 1.0));

        shader->setVec3Uniform("light.pos", lightPosCamSpace);
        shader->setVec3Uniform("light.La", light.ambient);
        shader->setVec3Uniform("light.Ld", light.diffuse);
        shader->setVec3Uniform("light.Ls", light.specular);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, sampler);
        grassTexture->bind();
        shader->setIntUniform("diffuseTex", 0);

        shader->setMat4Uniform("modelMatrix", grass->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix",
                                glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * grass->modelMatrix()))));
        grass->draw();

        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawTrees() {
        GLuint primitivesWritten;
        glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitivesWritten);

        shaderInstanced->use();

        shaderInstanced->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shaderInstanced->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        light.position =
                glm::vec3(glm::cos(phi) * glm::cos(theta), glm::sin(phi) * glm::cos(theta), glm::sin(theta)) * lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(light.position, 1.0));

        shaderInstanced->setVec3Uniform("light.pos",
                                         lightPosCamSpace);
        shaderInstanced->setVec3Uniform("light.La", light.ambient);
        shaderInstanced->setVec3Uniform("light.Ld", light.diffuse);
        shaderInstanced->setVec3Uniform("light.Ls", light.specular);

        glActiveTexture(GL_TEXTURE1);
        bufferTexCulled->bind();
        shaderInstanced->setIntUniform("texBuf", 1);

        glEnable(GL_BLEND);
        glBlendColor(0.7, 0.7, 0.7, 0.7);
        glBlendFunc(GL_CONSTANT_COLOR, GL_ONE_MINUS_CONSTANT_COLOR);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, sampler);
        leafTexture->bind();
        shaderInstanced->setIntUniform("diffuseTex", 0);

        shaderInstanced->setMat3Uniform("normalToCameraMatrix",
                                         glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
        leafs->drawInstanced(primitivesWritten);

        glDisable(GL_BLEND);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, sampler);
        barkTexture->bind();
        shaderInstanced->setIntUniform("diffuseTex", 0);

        shaderInstanced->setMat3Uniform("normalToCameraMatrix",
                                         glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix))));
        trunk->drawInstanced(primitivesWritten);

        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void cull(const ShaderProgramPtr &shader) {
        shader->use();

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader->setFloatUniform("treeH", treeHeight);
        shader->setFloatUniform("treeR", treeRadius);

        glEnable(GL_RASTERIZER_DISCARD);

        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, TF);

        glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);

        glBeginTransformFeedback(GL_POINTS);

        glBindVertexArray(cullVao);
        glDrawArrays(GL_POINTS, 0, nTrees);

        glEndTransformFeedback();

        glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

        glDisable(GL_RASTERIZER_DISCARD);
    }
};

int main() {
    srand(0);
    TreeApp app;
    app.start();

    return 0;
}
