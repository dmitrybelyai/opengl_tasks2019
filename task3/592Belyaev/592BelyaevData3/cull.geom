#version 330

layout(points) in;
layout(points, max_vertices = 1) out;

flat in int visible[1];

out vec3 position;

void main()
{
    //Здесь может быть произвольное условия отбрасывания
    if (visible[0] == 1)
    {
        position = gl_in[0].gl_Position.xyz;
    
        EmitVertex();        
        EndPrimitive();
    }
}
