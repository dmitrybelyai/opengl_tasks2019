include_directories(common)

set(SRC_FILES
    main.cpp
    common/DebugOutput.cpp
    common/Camera.cpp
    common/Application.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/Texture.cpp
)

set(HEADER_FILES
        common/Application.hpp
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        common/Texture.hpp
        common/DebugOutput.h
        )

add_compile_options(-DGLM_ENABLE_EXPERIMENTAL)

MAKE_OPENGL_TASK(592Belyaev 3 "${SRC_FILES}" "${HEADER_FILES}")